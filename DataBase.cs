﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace MySQLHelper
{
    public class DataBase
    {
        private string _dbName;
        private string _dbServer;

        private string _dbUser;
        private string _dbPass;

        private string _connectStr;
        private MySqlConnection _connectMySql;

        public DataBaseTable Table(string tableName)
        {
            return new DataBaseTable(tableName, _connectMySql);
        }
        public DataBase(string server, string name, string user, string password)
        {
            _dbName = name;
            _dbUser = user;

            _dbPass   = password;
            _dbServer = server;

            _connectStr = string.Format(@"server={0};userid={1};password={2};database={3}", _dbServer, _dbUser, _dbPass, _dbName);
            _connectMySql = new MySqlConnection(_connectStr);
        }

        public void Connect()
        {
            try
            {
                _connectMySql.Open();
            }
            catch (MySqlException error)
            {
                MessageBox.Show(error.ToString(), "Database connect");
            }
        }

        public DataTable Run(string value)
        {
            var res = new DataTable();
            try
            {
                var scr = new MySqlCommand(value, _connectMySql);
                scr.ExecuteNonQuery();

                var adapter = new MySqlDataAdapter(scr);
                adapter.Fill(res);

            }
            catch (MySqlException error)
            {
                MessageBox.Show(error.ToString(), "Running command");
            }

            return res;
        }

        public Dictionary<string, object> CallProcedure(string procedureName, List<Parameter> parametrs)
        {
            var res = new Dictionary<string, object>();

            try
            {
                var scr = new MySqlCommand
                {
                    Connection = _connectMySql,
                    CommandType = CommandType.StoredProcedure,
                    CommandText = procedureName
                };

                foreach (var item in parametrs)
                {
                    scr.Parameters.AddWithValue("@" + item.Name, item.Value);
                    scr.Parameters["@" + item.Name].Direction = item.Direction;
                }

                scr.ExecuteNonQuery();

                foreach (var item in parametrs.Where(item => item.Direction == ParameterDirection.Output))
                {
                    res.Add(item.Name, scr.Parameters["@" + item.Name].Value);
                }

            }
            catch (MySqlException error)
            {
                MessageBox.Show(error.ToString(), "Running command");
            }

            return res;
        }

        public void Disconnect()
        {
            try
            {
                _connectMySql.Close();
            }
            catch (MySqlException error)
            {
                MessageBox.Show(error.ToString(), "Database disconnect");
            }
        }
    }

}
