# README #

This is helper for mysql + c#. Easy to use

## What are you need ##

* MySql.Data.dll - part of mysql connector
* Windows Forms application (.net 4+)

## How to connect ##

* Add MySql.Data.dll to references
* Add 3 files to ur project
* Add namespace MySQLHelper
* Enjoy

## Using ##
Initialize, Connect, Disconnect
```
var dBase = new DataBase("localhost", "name", "user", "password");    
dBase.Connect();    
dBase.Disconnect();
```

Operations with table
```
//Get Table
var table = dBase.Table("table_name");

//Get entry
var entry = table.GetEntry("id='1'");

//Edit entry
table.EditEntry("id='1'", "text='new text'");

//Add entry
table.AddEntry("text='text', param1='value1', param2 = 'value2'");

//Delete entry
table.DeleteEntry("id='1'");
```
Calling procedures
```
var parameters = new List<Parameter>
{
   new Parameter("parameter_name", MySqlDbType.Int32, ParameterDirection.Output)
};
var res = (int)dBase.CallProcedure("procedure_name", parameters)["parameter_name"];
```
Running another code
```
dBase.Run("CREATE TABLE table_name (Id int, Name text);");
```