﻿using System.Data;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace MySQLHelper
{
    public class DataBaseTable
    {
        private readonly string _tableName;
        private readonly
                MySqlConnection _connectMySql;
        private MySqlDataAdapter _dataAdapter;
        private MySqlCommand _dataCommand;

        public DataBaseTable(string name, MySqlConnection connect)
        {
            _tableName = name;
            _connectMySql = connect;
        }

        public DataTable Data(string whereValue = null)
        {
            var dataRes = new DataTable();
            var dataRequest = "select * from " + _tableName;

            if (!string.IsNullOrEmpty(whereValue))
                dataRequest += " where " + whereValue;

            try
            {
                _dataCommand = new MySqlCommand(dataRequest, _connectMySql);
                _dataCommand.ExecuteNonQueryAsync();
                _dataAdapter = new MySqlDataAdapter(_dataCommand);
                _dataAdapter.Fill(dataRes);
            }
            catch (MySqlException error)
            {
                MessageBox.Show(error.ToString(), string.Format("Table {0} getting", _tableName));
            }

            return dataRes;
        }

        public string[,] ToArray()
        {
            var data = Data().Select();
            if (data.Length == 0) return null;

            var res = new string[data.Length, data[0].ItemArray.Length];

            for (int i = 0; i < data.Length; i++)
            {
                for (int j = 0; j < data[i].ItemArray.Length; j++)
                {
                    res[i, j] = data[i].ItemArray[j].ToString();
                }
            }

            return res;
        }

        public DataBaseTable EditEntry(string whereValue, string setValue)
        {
            try
            {
                var scr = new MySqlCommand(string.Format("update {0} set {1} where {2}", _tableName, setValue, whereValue), _connectMySql);
                scr.ExecuteNonQuery();
            }
            catch (MySqlException error)
            {
                MessageBox.Show(error.ToString(), string.Format("Table {0} editing", _tableName));
            }
            return this;
        }

        public DataBaseTable DeleteEntry(string whereValue)
        {
            try
            {
                var scr = new MySqlCommand(string.Format("delete from {0} where {1}", _tableName, whereValue), _connectMySql);
                scr.ExecuteNonQuery();
            }
            catch (MySqlException error)
            {
                MessageBox.Show(error.ToString(), string.Format("Table {0} deleting", _tableName));
            }
            return this;
        }

        public DataBaseTable AddEntry(string setValue)
        {
            try
            {
                var scr = new MySqlCommand(string.Format("insert {0} set {1} ", _tableName, setValue), _connectMySql);
                scr.ExecuteNonQuery();
            }
            catch (MySqlException error)
            {
                MessageBox.Show(error.ToString(), "Table " + _tableName + " editing");
            }

            return this;
        }

        public string[,] GetEntry(string whereValue)
        {
            var data = Data(whereValue).Select();
            if (data.Length == 0) return null;

            var res = new string[data.Length, data[0].ItemArray.Length];

            for (int i = 0; i < data.Length; i++)
            {
                for (int j = 0; j < data[i].ItemArray.Length; j++)
                {
                    res[i, j] = data[i].ItemArray[j].ToString();
                }
            }

            return res;
        }
    }

}
