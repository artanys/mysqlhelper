﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MySQLHelper
{
    public class Parameter
    {
        public string Name { get; set; }
        public object Value { get; set; }
        public ParameterDirection Direction { get; set; }
        public Parameter(string name, object value, ParameterDirection direction)
        {
            Name = name;
            Value = value;
            Direction = direction;
        }
    }

}
